package com.example.usuario.deber;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Conocimientos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conocimientos);
        regreso();
    }


    public void regreso(){
        ActionBar action= getSupportActionBar();
        if(action!= null){
            action.setDisplayHomeAsUpEnabled(true);
            action.setTitle("Regresar");
        }
    }


}
